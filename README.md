# carrefour-kata-api

## Description

Le Kata est réalisé dans le cadre de l'entretien technique du client Carrefour

## description fonctionnelle

Le mini-projet permet de créer une application de gestion des reservations de crénaux par mode de livraison demandé.

### Régles de gestion :

1 - Un utilisateur connecté, peut recupérer une liste de crénaux par mode de livraison

2 - Les modes de livraison disponibles sont :

- DRIVE
- DELIVERY
- DELIVERY_TODAY
- DELIVERY ASAP

3 - un créneau horaire possède :

- une date de début
- une date de fin
- une capacité : nombre de reservations possibles par créneau

4 - un créneau sera envoyé uniquement dans le cas ou la date du créneau est dans le future

5 - un utilisateur doit accepter les consentements de l'application pour faire une reservation

6 - une reservation sera identifiée par l'email d'utilisateur (information obligatoire à saisir)

7 - Aucune contrainte sur le nombre de reservation possibles par adresse emails

8 - Impossible d'annuler un créneau déja passé (date dans le passé)

## description technique

- Test unitaire pour les services, avec l'utilisation de Jupiter + Mockito
- Une approche de test d'integrations est integrer pour permettre de tester le parcour complet de l'API
- Le lancement de l'application se fait par le biais du fichier docker-compose.yml qui permet de lancer le service
  postgresql et créer un environnement d'execution de l'application
- Utilisation de liquibase pour la gestion de la migration incrémentale des script de création de la BD
- La base de données utilisée est PostgreSQL (la preférée des developpeurs)
- Spring data pour la gestion de persistence
- Hateoas est rajouté uniquement pour tester l'approche sur les endpoints de reservations
- Le composer permet de créer des variables d'environnements (utilisation de la dépendance spring-boot-docker-compose)
- La doc openAPI est accessible depuis http://localhost:8080/swagger-ui/index.html#/