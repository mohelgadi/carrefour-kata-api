package com.carrefour.kata.reservation.service;

import com.carrefour.kata.enums.ModeLivraison;
import com.carrefour.kata.enums.StatutReservation;
import com.carrefour.kata.exception.ConsentementNotAcceptedException;
import com.carrefour.kata.exception.OperationInacceptableException;
import com.carrefour.kata.exception.ResourceIntrouvableException;
import com.carrefour.kata.persistence.entity.Creneau;
import com.carrefour.kata.persistence.entity.Reservation;
import com.carrefour.kata.persistence.repository.CreneauRepository;
import com.carrefour.kata.persistence.repository.ReservationRepository;
import com.carrefour.kata.reservation.dto.ReservationDto;
import com.carrefour.kata.reservation.dto.ReservationInputDto;
import com.carrefour.kata.reservation.mapper.ReservationMapper;
import com.carrefour.kata.reservation.mapper.assembler.ReservationModelAssembler;
import com.carrefour.kata.reservation.resource.ReservationResources;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
class ReservationServiceTest {
    @Mock
    private ReservationRepository reservationRepository;
    @Mock
    private ReservationResources reservationResources;
    @Mock
    private ReservationModelAssembler reservationModelAssembler;
    @Mock
    private CreneauRepository creneauRepository;
    @InjectMocks
    private ReservationService reservationService;

    @Test
    void testCreateReservation_shouldThrowException_ifUserNotAcceptedConsentement() {
        ReservationInputDto reservationInputDto = new ReservationInputDto()
                .setAcceptConsentement(false)
                .setEmail("email@gmail.com")
                .setCreneauId(UUID.randomUUID());

        Assertions.assertThrows(ConsentementNotAcceptedException.class,
                () -> reservationService.createReservation(reservationInputDto));
    }

    @Test
    void testCreateReservation_shouldCreateIt_ifCreneauExistsAndConsentementOk() {
        UUID creneauId = UUID.randomUUID();
        ReservationInputDto reservationInputDto = new ReservationInputDto()
                .setAcceptConsentement(true)
                .setEmail("email@gmail.com")
                .setCreneauId(creneauId);

        Mockito.when(creneauRepository.findAvailableCreneauById(creneauId))
                .thenReturn(Optional.of((Creneau) new Creneau()
                        .setModeLivraison(ModeLivraison.DELIVERY_TODAY)
                        .setDateDebut(LocalDateTime.now())
                        .setDateFin(LocalDateTime.now())
                        .setCapacity(5)
                        .setId(creneauId)));

        ArgumentCaptor<Reservation> creneauArgumentCaptor = ArgumentCaptor.forClass(Reservation.class);


        reservationService.createReservation(reservationInputDto);

        Mockito.verify(reservationRepository).save(creneauArgumentCaptor.capture());
        Reservation reservation = creneauArgumentCaptor.getValue();

        Assertions.assertEquals(StatutReservation.ACTIVE, reservation.getStatut());
        Assertions.assertEquals(reservationInputDto.getEmail(), reservation.getEmail());
        Assertions.assertEquals(ModeLivraison.DELIVERY_TODAY, reservation.getCreneau().getModeLivraison());
        Assertions.assertEquals(4, reservation.getCreneau().getCapacity());
    }


    @Test
    void testGetReservation_shouldThrowIntrouvableResource_ifNotExists() {
        final UUID reservationId = UUID.randomUUID();
        Assertions.assertThrows(ResourceIntrouvableException.class, () -> reservationService.getReservationById(reservationId));
    }

    @Test
    void testGetReservation_shouldReturnValud_ifExists() {
        final UUID reservationId = UUID.randomUUID();

        Reservation reservation = (Reservation) new Reservation()
                .setEmail("mmm@gmail.com")
                .setStatut(StatutReservation.ACTIVE)
                .setAcceptConsentement(true)
                .setCreneau((Creneau) new Creneau()
                        .setCapacity(3)
                        .setModeLivraison(ModeLivraison.DELIVERY_TODAY)
                        .setId(UUID.randomUUID()))
                .setCreationDate(Instant.now());


        Mockito.when(reservationRepository.findById(reservationId))
                .thenReturn(Optional.of(reservation));

        Mockito.when(reservationModelAssembler.toModel(Mockito.any())).thenReturn(ReservationMapper.toDto(reservation));

        ReservationDto reservationDto =
                reservationService.getReservationById(reservationId);

        Assertions.assertNotNull(reservationDto);
        Assertions.assertEquals("mmm@gmail.com", reservationDto.getEmail());
        Assertions.assertEquals(3, reservationDto.getCreneau().getCapacity());
        Assertions.assertEquals(ModeLivraison.DELIVERY_TODAY, reservationDto.getCreneau().getModeLivraison());
    }

    @Test
    void testCancelReservation_shouldThrowOperationInacceptable_ifIsAlreadyCancelled() {
        final UUID reservationId = UUID.randomUUID();

        Mockito.when(reservationRepository.findActiveReservationToCancelById(reservationId))
                .thenReturn(Optional.empty());

        Assertions.assertThrows(OperationInacceptableException.class, () -> reservationService.cancelReservation(reservationId));

    }

    @Test
    void testCancelReservation_shouldDoCancellationOperation_ifNominalCase() {
        final UUID reservationId = UUID.randomUUID();

        Reservation reservation = (Reservation) new Reservation()
                .setEmail("mmm@gmail.com")
                .setStatut(StatutReservation.ACTIVE)
                .setAcceptConsentement(true)
                .setCreneau((Creneau) new Creneau()
                        .setCapacity(3)
                        .setModeLivraison(ModeLivraison.DELIVERY_TODAY)
                        .setId(UUID.randomUUID()))
                .setCreationDate(Instant.now());

        Mockito.when(reservationRepository.save(Mockito.any()))
                .thenReturn(reservation);

        Mockito.when(reservationRepository.findActiveReservationToCancelById(reservationId))
                .thenReturn(Optional.of(reservation));

        ArgumentCaptor<Reservation> creneauArgumentCaptor = ArgumentCaptor.forClass(Reservation.class);

        reservationService.cancelReservation(reservationId);

        Mockito.verify(reservationRepository, Mockito.times(1))
                .save(creneauArgumentCaptor.capture());

        Reservation reservationCaptured = creneauArgumentCaptor.getValue();

        Assertions.assertEquals(StatutReservation.CANCELLED, reservationCaptured.getStatut());
        Assertions.assertEquals(4, reservationCaptured.getCreneau().getCapacity());
    }
}
