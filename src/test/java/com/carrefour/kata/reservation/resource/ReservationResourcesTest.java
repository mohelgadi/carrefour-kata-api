package com.carrefour.kata.reservation.resource;

import com.carrefour.kata.CustomPostgreSQLContainer;
import com.carrefour.kata.enums.StatutReservation;
import com.carrefour.kata.reservation.dto.ReservationDto;
import com.carrefour.kata.reservation.dto.ReservationInputDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlMergeMode;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@SqlMergeMode(SqlMergeMode.MergeMode.MERGE)
public class ReservationResourcesTest extends CustomPostgreSQLContainer {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    @Sql(value = "classpath:db/create_crenaux_in_past.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testCreateReservation_shouldReturnNotFound_whenNoCrenauIsPassed() throws Exception {
        final ResultActions createReservationResultAction =
                mockMvc.perform(
                        post("/reservations")
                                .content(objectMapper.writeValueAsString(new ReservationInputDto()
                                        .setAcceptConsentement(true)
                                        .setEmail("testEmail@Gmail.com")
                                        .setCreneauId(UUID.fromString("7e23c640-cabb-44ba-8b1f-9b1984aa8d18"))))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));
        // crenaux not exists
        createReservationResultAction
                .andExpect(status().isOk());
    }

    @Test
    @Sql(value = "classpath:db/init_creneaux_in_futur.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testCreateReservation_shouldUnauthorizeRequest_ifConsentementNotAccepted() throws Exception {
        final ResultActions createReservationResultAction =
                mockMvc.perform(
                        post("/reservations")
                                .content(objectMapper.writeValueAsString(new ReservationInputDto()
                                        .setAcceptConsentement(false)
                                        .setEmail("testEmail@Gmail.com")
                                        .setCreneauId(UUID.fromString("6fedb550-22e3-445b-bc78-fd9a2ef274bb"))))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));
        // should not authorize request
        createReservationResultAction
                .andExpect(status().isUnauthorized());
    }

    @Test
    void testCreateReservation_shouldReturnBadRequest_ifEmailIsMalFormed() throws Exception {
        final ResultActions createReservationResultAction =
                mockMvc.perform(
                        post("/reservations")
                                .content(objectMapper.writeValueAsString(new ReservationInputDto()
                                        .setAcceptConsentement(false)
                                        .setEmail("testEmailGmail.com")
                                        .setCreneauId(UUID.fromString("6fedb550-22e3-445b-bc78-fd9a2ef274bb"))))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));
        // should not authorize request
        createReservationResultAction
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"errors\":[\"Invalid Email\"]}"));
    }

    @Test
    @Sql(value = "classpath:db/init_creneaux_in_futur.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testCreateReservation_shouldCreationReservation_whenCrenauIsAvailableInFutur() throws Exception {
        final ResultActions createReservationResultAction =
                mockMvc.perform(
                        post("/reservations")
                                .content(objectMapper.writeValueAsString(new ReservationInputDto()
                                        .setAcceptConsentement(true)
                                        .setEmail("testEmail@Gmail.com")
                                        .setCreneauId(UUID.fromString("6fedb550-22e3-445b-bc78-fd9a2ef274bb"))))
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));
        // crenaux order accepted
        createReservationResultAction
                .andExpect(status().isOk());

        final ReservationDto reservationDto = objectMapper.readValue(createReservationResultAction.andReturn().getResponse().getContentAsString(), ReservationDto.class);

        final ResultActions getReservationById =
                mockMvc.perform(
                        get("/reservations/{reservationId}", reservationDto.getId())
                                .accept(MediaType.APPLICATION_JSON));
        // crenaux reservation accepted
        // reservation creacted
        // capacity decremented
        getReservationById
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").exists())
                .andExpect(jsonPath("$.creneau.capacity").value(9));
    }

    @Test
    @Sql(value = "classpath:db/init_creneaux_in_futur.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/create_reservation.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testGetReservationById_shouldReturnValue_ifExists() throws Exception {
        final ResultActions getReservationResultAction =
                mockMvc.perform(
                        get("/reservations/7e23c640-cabb-44ba-8b1f-9b1984aa8d18")
                                .accept(MediaType.APPLICATION_JSON));
        // should not authorize request
        getReservationResultAction
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statut").value(StatutReservation.ACTIVE.toString()))
                .andExpect(jsonPath("$.id").value("7e23c640-cabb-44ba-8b1f-9b1984aa8d18"));
    }

    @Test
    @Sql(value = "classpath:db/init_creneaux_in_futur.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/create_reservation.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testCancelReservation_shouldCancelReservation_whenExistsAndInTheFutur() throws Exception {
        final ResultActions deleteReservationResultAction =
                mockMvc.perform(
                        delete("/reservations/7e23c640-cabb-44ba-8b1f-9b1984aa8d18")
                                .accept(MediaType.APPLICATION_JSON));
        // should not authorize request
        deleteReservationResultAction
                .andExpect(status().isOk());

        final ResultActions getReservationResultAction =
                mockMvc.perform(
                        get("/reservations/7e23c640-cabb-44ba-8b1f-9b1984aa8d18")
                                .accept(MediaType.APPLICATION_JSON));
        // should not authorize request
        Assertions.assertTrue(getReservationResultAction
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.statut").value(StatutReservation.CANCELLED.toString()))
                .andExpect(jsonPath("$.id").value("7e23c640-cabb-44ba-8b1f-9b1984aa8d18"))
                .andReturn()
                .getResponse()
                .getContentAsString()
                .contains("/reservations/7e23c640-cabb-44ba-8b1f-9b1984aa8d18"));
    }

    @Test
    @Sql(value = "classpath:db/create_crenaux_in_past.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/create_reservation_creneau_in_past.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testCancelReservation_shouldThrowInaccepteableException_whenExistsButCrenauInThePast() throws Exception {
        final ResultActions deleteReservationResultAction =
                mockMvc.perform(
                        delete("/reservations/7e23c640-cabb-44ba-8b1f-9b1984aa8d18")
                                .accept(MediaType.APPLICATION_JSON));
        // should not authorize request
        deleteReservationResultAction
                .andExpect(status().isNotAcceptable());
    }
}
