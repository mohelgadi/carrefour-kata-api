package com.carrefour.kata.creneau.service;

import com.carrefour.kata.creneau.dto.CreneauDto;
import com.carrefour.kata.enums.ModeLivraison;
import com.carrefour.kata.persistence.entity.Creneau;
import com.carrefour.kata.persistence.repository.CreneauRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@ExtendWith(MockitoExtension.class)
class CreneauServiceTest {

    @Mock
    private CreneauRepository creneauRepository;

    @InjectMocks
    private CreneauService creneauService;

    @Test
    void testGetCrenauxByModeLivraison_shouldReturnEmptyPage_whenNotCreneauAvailable() {
        Mockito.when(creneauRepository
                        .findAvailableCrenauxByModeLivraison(Mockito.any(), Mockito.any()))
                .thenReturn(Page.empty());
        Page<CreneauDto> creneauDtos = creneauService.getCrenauxByModeLivraison(ModeLivraison.DELIVERY, Pageable.ofSize(10));
        Assertions.assertEquals(0, creneauDtos.getTotalElements());
    }

    @Test
    void testGetCrenauxByModeLivraison_shouldReturnCreneaux_whenNotCreneauAvailables() {
        // Given
        Page<Creneau> page = new PageImpl<>(List.of((Creneau) new Creneau()
                .setDateDebut(LocalDateTime.now())
                .setDateFin(LocalDateTime.now())
                .setDescription("DESC")
                .setCapacity(10)
                .setModeLivraison(ModeLivraison.DELIVERY)
                .setId(UUID.randomUUID())), Pageable.ofSize(1), 1);
        // when
        Mockito.when(creneauRepository
                        .findAvailableCrenauxByModeLivraison(Mockito.any(), Mockito.any()))
                .thenReturn(page);

        // then
        Page<CreneauDto> creneauDtos = creneauService.getCrenauxByModeLivraison(ModeLivraison.DELIVERY, Pageable.ofSize(10));

        // assertions
        Assertions.assertEquals(1, creneauDtos.getTotalElements());
        Assertions.assertEquals(ModeLivraison.DELIVERY, creneauDtos.getContent().get(0).getModeLivraison());
        Assertions.assertEquals(10, creneauDtos.getContent().get(0).getCapacity());
        Assertions.assertEquals("DESC", creneauDtos.getContent().get(0).getDescription());


    }
}
