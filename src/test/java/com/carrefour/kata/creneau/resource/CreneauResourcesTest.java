package com.carrefour.kata.creneau.resource;

import com.carrefour.kata.CustomPostgreSQLContainer;
import com.carrefour.kata.enums.ModeLivraison;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class CreneauResourcesTest extends CustomPostgreSQLContainer {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void testGetCrenauxByModeLivraison_shouldReturnEmptyPage_whenNoCrenauIsAvailable() throws Exception {
        final ResultActions createOrderResultAction =
                mockMvc.perform(
                        get("/creneaux")
                                .param("modeLivraison", ModeLivraison.DELIVERY_ASAP.toString())
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));
        createOrderResultAction
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

    @Test
    @Sql(value = "classpath:db/init_creneaux_in_futur.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testGetCrenauxByModeLivraison_shouldReturnPage_whenCrenauAvailable() throws Exception {

        final ResultActions createOrderResultAction =
                mockMvc.perform(
                        get("/creneaux")
                                .param("modeLivraison", ModeLivraison.DRIVE.toString())
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));
        createOrderResultAction
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.totalElements").value(1))
                .andExpect(
                        jsonPath("$.content[0].description")
                                .value("Veuillez patientez devant la portiere"))
                .andExpect(
                        jsonPath("$.content[0].capacity")
                                .value(10))
                .andExpect(
                        jsonPath("$.content[0].modeLivraison")
                                .value(ModeLivraison.DRIVE.toString()))
                .andExpect(jsonPath("$.content", hasSize(1)));
    }

    @Test
    @Sql(value = "classpath:db/create_unavailable_creneau_capacity0.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testGetCrenauxByModeLivraison_shouldReturnEmptyPage_whenCreneauExistsButUnavailable_capacityReserved() throws Exception {

        final ResultActions createOrderResultAction =
                mockMvc.perform(
                        get("/creneaux")
                                .param("modeLivraison", ModeLivraison.DELIVERY_TODAY.toString())
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));
        createOrderResultAction
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(0)));
    }

    @Test
    @Sql(value = "classpath:db/create_crenaux_in_past.sql",
            executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = "classpath:db/clean_up.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    void testGetCrenauxByModeLivraison_shouldReturnEmptyPage_whenCreneauExistsButUnavailable_dateInPast() throws Exception {
        // crénaux in the past should not be returned by GET /creneaux
        final ResultActions createOrderResultAction =
                mockMvc.perform(
                        get("/creneaux")
                                .param("modeLivraison", ModeLivraison.DELIVERY_TODAY.toString())
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON));
        createOrderResultAction
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(0)));
    }
}
