package com.carrefour.kata;

import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.containers.wait.strategy.LogMessageWaitStrategy;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

@SpringBootTest
public class CustomPostgreSQLContainer {

    public static PostgreSQLContainer postgresqlContainer =
            new PostgreSQLContainer(
                    DockerImageName.parse("postgres:13-alpine")
                            .asCompatibleSubstituteFor("postgres"))
                    .withDatabaseName("db")
                    .withUsername("test_user")
                    .withPassword("test_password");
    ;

    @BeforeAll
    public static void setUp() {
        postgresqlContainer.withInitScript("./db/init.sql");
        postgresqlContainer.setWaitStrategy(
                new LogMessageWaitStrategy()
                        .withRegEx(".*database system is ready to accept connections.*\\s")
                        .withTimes(1)
                        .withStartupTimeout(Duration.of(60, ChronoUnit.SECONDS)));
        postgresqlContainer.start();
    }

    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.datasource.url", postgresqlContainer::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.username", postgresqlContainer::getUsername);
        dynamicPropertyRegistry.add("spring.datasource.password", postgresqlContainer::getPassword);
        dynamicPropertyRegistry.add(
                "spring.datasource.driver-class-name", postgresqlContainer::getDriverClassName);
    }
}
