package com.carrefour.kata.persistence.entity;

import com.carrefour.kata.enums.StatutReservation;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicUpdate;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(schema = "kata")
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Reservation extends CarrefourKataEntity {
    @NotNull
    @Email
    private String email;

    @NotNull
    private boolean acceptConsentement;

    @NotNull
    @Enumerated(EnumType.STRING)
    private StatutReservation statut;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "creneau_id")
    private Creneau creneau;

}
