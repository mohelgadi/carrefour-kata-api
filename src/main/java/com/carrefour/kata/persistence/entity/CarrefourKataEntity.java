package com.carrefour.kata.persistence.entity;

import jakarta.persistence.Column;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.MappedSuperclass;
import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.time.Instant;
import java.util.UUID;


@Getter
@Setter
@MappedSuperclass
@Accessors(chain = true)
@EqualsAndHashCode(of = "id")
@DynamicUpdate
@EntityListeners(AuditingEntityListener.class)
public abstract class CarrefourKataEntity {
    @NotNull
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "uuid")
    protected UUID id;

    @CreatedDate
    @Column(name = "creation_date", nullable = false, updatable = false)
    protected Instant creationDate;

    @LastModifiedDate
    @Column(name = "modification_date", nullable = false, updatable = false)
    protected Instant modificationDate;
}
