package com.carrefour.kata.persistence.entity;

import com.carrefour.kata.enums.ModeLivraison;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.DynamicUpdate;

import java.time.LocalDateTime;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(schema = "kata")
@DynamicUpdate
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Creneau extends CarrefourKataEntity {
    @NotNull
    @Enumerated(EnumType.STRING)
    private ModeLivraison modeLivraison;
    @NotNull
    private LocalDateTime dateDebut;
    @NotNull
    private LocalDateTime dateFin;
    private String description;
    private int capacity;

    public Creneau incrementCapacity() {
        this.capacity = this.capacity + 1;
        return this;
    }

    public Creneau decrementCapacity() {
        this.capacity = this.capacity - 1;
        return this;
    }
}
