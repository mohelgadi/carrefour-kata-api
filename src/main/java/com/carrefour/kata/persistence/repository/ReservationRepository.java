package com.carrefour.kata.persistence.repository;

import com.carrefour.kata.enums.StatutReservation;
import com.carrefour.kata.persistence.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

public interface ReservationRepository extends JpaRepository<Reservation, UUID> {
    Optional<Reservation> findByIdAndStatutAndCreneauDateDebutGreaterThan(final UUID id, final StatutReservation statutReservation, final LocalDateTime localDateTime);

    default Optional<Reservation> findActiveReservationToCancelById(final UUID reservationId) {
        return findByIdAndStatutAndCreneauDateDebutGreaterThan(reservationId, StatutReservation.ACTIVE, LocalDateTime.now());
    }
}
