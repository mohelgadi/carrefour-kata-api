package com.carrefour.kata.persistence.repository;

import com.carrefour.kata.enums.ModeLivraison;
import com.carrefour.kata.persistence.entity.Creneau;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

public interface CreneauRepository extends JpaRepository<Creneau, UUID> {

    default Page<Creneau> findAvailableCrenauxByModeLivraison(final ModeLivraison modeLivraison, final Pageable pageable) {
        return findByModeLivraisonAndCapacityGreaterThanAndDateDebutGreaterThan(modeLivraison, 0, LocalDateTime.now(), pageable);
    }

    default Optional<Creneau> findAvailableCreneauById(final UUID creneauId) {
        return findByIdAndCapacityGreaterThan(creneauId, 0);
    }

    Page<Creneau> findByModeLivraisonAndCapacityGreaterThanAndDateDebutGreaterThan(final ModeLivraison modeLivraison, final long minCapacity, final LocalDateTime minDate, final Pageable pageable);

    Optional<Creneau> findByIdAndCapacityGreaterThan(final UUID creneauId, final long minCapacity);

}
