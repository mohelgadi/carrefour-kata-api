package com.carrefour.kata.exception;

import lombok.experimental.StandardException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@StandardException
@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class OperationInacceptableException extends RuntimeException {
}
