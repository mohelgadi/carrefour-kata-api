package com.carrefour.kata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class KataApplication {
    public static void main(final String[] args) {
        SpringApplication.run(KataApplication.class, args);
    }
}
