package com.carrefour.kata.enums;

public enum StatutReservation {
    ACTIVE, CANCELLED;
}
