package com.carrefour.kata.enums;

public enum ModeLivraison {
    DRIVE, DELIVERY, DELIVERY_TODAY, DELIVERY_ASAP;
}
