package com.carrefour.kata.creneau.service;

import com.carrefour.kata.creneau.dto.CreneauDto;
import com.carrefour.kata.creneau.mapper.CreneauMapper;
import com.carrefour.kata.enums.ModeLivraison;
import com.carrefour.kata.persistence.repository.CreneauRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CreneauService {
    private final CreneauRepository creneauRepository;

    @Transactional(readOnly = true)
    public Page<CreneauDto> getCrenauxByModeLivraison(@NonNull final ModeLivraison modeLivraison, final Pageable pageable) {
        return creneauRepository.findAvailableCrenauxByModeLivraison(modeLivraison, pageable).map(CreneauMapper::toDto);
    }
}
