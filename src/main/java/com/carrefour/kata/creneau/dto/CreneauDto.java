package com.carrefour.kata.creneau.dto;

import com.carrefour.kata.enums.ModeLivraison;
import jakarta.annotation.Nullable;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class CreneauDto {
    @NotNull
    private UUID id;
    private ModeLivraison modeLivraison;
    @NotNull
    private LocalDateTime dateDebut;
    @NotNull
    private LocalDateTime dateFin;
    @Nullable
    private String description;
    private int capacity;
}
