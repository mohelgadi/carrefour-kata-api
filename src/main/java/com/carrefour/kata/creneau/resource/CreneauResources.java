package com.carrefour.kata.creneau.resource;

import com.carrefour.kata.creneau.dto.CreneauDto;
import com.carrefour.kata.creneau.service.CreneauService;
import com.carrefour.kata.enums.ModeLivraison;
import io.swagger.v3.oas.annotations.Operation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/creneaux")
@RequiredArgsConstructor
public class CreneauResources {
    private final CreneauService creneauService;

    @Operation(description = "Get creneau list by mode de livraison")
    @GetMapping
    public ResponseEntity<Page<CreneauDto>> getCrenauxByModeLivraison(@RequestParam final ModeLivraison modeLivraison, final Pageable pageable) {
        return ResponseEntity.ok(creneauService.getCrenauxByModeLivraison(modeLivraison, pageable));
    }
}
