package com.carrefour.kata.creneau.mapper;

import com.carrefour.kata.creneau.dto.CreneauDto;
import com.carrefour.kata.persistence.entity.Creneau;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CreneauMapper {
    public static CreneauDto toDto(@NonNull final Creneau creneau) {
        return new CreneauDto().setId(creneau.getId())
                .setDateDebut(creneau.getDateDebut())
                .setDateFin(creneau.getDateFin())
                .setModeLivraison(creneau.getModeLivraison())
                .setDescription(creneau.getDescription())
                .setCapacity(creneau.getCapacity());
    }
}