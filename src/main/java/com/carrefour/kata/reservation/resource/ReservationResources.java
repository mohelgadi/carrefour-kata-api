package com.carrefour.kata.reservation.resource;

import com.carrefour.kata.reservation.dto.ReservationDto;
import com.carrefour.kata.reservation.dto.ReservationInputDto;
import com.carrefour.kata.reservation.service.ReservationService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping(path = "/reservations")
@RequiredArgsConstructor
public class ReservationResources {

    private final ReservationService reservationService;

    @Operation(description = "Create Reservation")
    @PostMapping
    public ResponseEntity<ReservationDto> createReservation(@RequestBody @Valid final ReservationInputDto reservationInput) {
        return ResponseEntity.of(reservationService.createReservation(reservationInput));
    }

    @Operation(description = "Get reservation by id")
    @GetMapping("/{reservationId}")
    public ReservationDto getReservationById(@PathVariable final UUID reservationId) {
        return reservationService.getReservationById(reservationId);
    }


    @Operation(description = "Cancel Reservation by id. the reservation should be active")
    @DeleteMapping("/{reservationId}")
    public void cancelReservation(@PathVariable final UUID reservationId) {
        reservationService.cancelReservation(reservationId);
    }
}
