package com.carrefour.kata.reservation.dto;

import com.carrefour.kata.creneau.dto.CreneauDto;
import com.carrefour.kata.enums.StatutReservation;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.hateoas.RepresentationModel;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
public class ReservationDto extends RepresentationModel<ReservationDto> {
    @NotNull
    private UUID id;
    @NotNull
    private CreneauDto creneau;

    private LocalDateTime dateCreation;

    @NotNull
    @NotEmpty
    protected String email;

    protected boolean acceptConsentement;

    @NotNull
    private StatutReservation statut;
}
