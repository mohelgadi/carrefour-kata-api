package com.carrefour.kata.reservation.mapper.assembler;

import com.carrefour.kata.persistence.entity.Reservation;
import com.carrefour.kata.reservation.dto.ReservationDto;
import com.carrefour.kata.reservation.mapper.ReservationMapper;
import com.carrefour.kata.reservation.resource.ReservationResources;
import lombok.NonNull;
import org.springframework.hateoas.server.mvc.RepresentationModelAssemblerSupport;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class ReservationModelAssembler extends RepresentationModelAssemblerSupport<Reservation, ReservationDto> {

    public ReservationModelAssembler() {
        super(ReservationResources.class, ReservationDto.class);
    }

    @Override
    public @NonNull ReservationDto toModel(@NonNull final Reservation reservation) {
        return ReservationMapper
                .toDto(reservation)
                .add(WebMvcLinkBuilder.linkTo(getControllerClass()).slash(reservation.getId()).withSelfRel());
    }
}
