package com.carrefour.kata.reservation.mapper;

import com.carrefour.kata.creneau.mapper.CreneauMapper;
import com.carrefour.kata.persistence.entity.Creneau;
import com.carrefour.kata.persistence.entity.Reservation;
import com.carrefour.kata.reservation.dto.ReservationDto;
import com.carrefour.kata.reservation.dto.ReservationInputDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.time.LocalDateTime;
import java.time.ZoneId;


@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ReservationMapper {
    public static ReservationDto toDto(@NonNull final Reservation reservation) {
        return new ReservationDto()
                .setCreneau(CreneauMapper.toDto(reservation.getCreneau()))
                .setId(reservation.getId())
                .setDateCreation(LocalDateTime.ofInstant(reservation.getCreationDate(), ZoneId.systemDefault()))
                .setAcceptConsentement(reservation.isAcceptConsentement())
                .setStatut(reservation.getStatut())
                .setEmail(reservation.getEmail());
    }

    public static Reservation toEntity(@NonNull final ReservationInputDto reservationInputDto, @NonNull final Creneau creneau) {
        return new Reservation()
                .setAcceptConsentement(reservationInputDto.isAcceptConsentement())
                .setEmail(reservationInputDto.getEmail())
                .setCreneau(creneau);
    }
}