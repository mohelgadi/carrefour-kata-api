package com.carrefour.kata.reservation.service;

import com.carrefour.kata.enums.StatutReservation;
import com.carrefour.kata.exception.ConsentementNotAcceptedException;
import com.carrefour.kata.exception.OperationInacceptableException;
import com.carrefour.kata.exception.ResourceIntrouvableException;
import com.carrefour.kata.persistence.entity.Creneau;
import com.carrefour.kata.persistence.entity.Reservation;
import com.carrefour.kata.persistence.repository.CreneauRepository;
import com.carrefour.kata.persistence.repository.ReservationRepository;
import com.carrefour.kata.reservation.dto.ReservationDto;
import com.carrefour.kata.reservation.dto.ReservationInputDto;
import com.carrefour.kata.reservation.mapper.ReservationMapper;
import com.carrefour.kata.reservation.mapper.assembler.ReservationModelAssembler;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
public class ReservationService {
    private final ReservationRepository reservationRepository;
    private final CreneauRepository creneauRepository;
    private final ReservationModelAssembler reservationModelAssembler;

    @Transactional
    public Optional<ReservationDto> createReservation(@NonNull final ReservationInputDto reservationInputDto) {
        final UUID creneauId = reservationInputDto.getCreneauId();
        // verify that user accepted conditions
        if (!reservationInputDto.isAcceptConsentement()) {
            throw new ConsentementNotAcceptedException("Please accept conditions to validate your reservation");
        }
        return creneauRepository.findAvailableCreneauById(creneauId)
                .map(creneau -> createReservation(reservationInputDto, creneau))
                .map(reservationModelAssembler::toModel);
    }


    @Transactional(readOnly = true)
    public ReservationDto getReservationById(@NonNull final UUID reservationId) {
        return reservationRepository.findById(reservationId)
                .map(reservationModelAssembler::toModel)
                .orElseThrow(ResourceIntrouvableException::new);
    }

    @Transactional
    public void cancelReservation(@NonNull final UUID reservationId) {
        reservationRepository.findActiveReservationToCancelById(reservationId)
                .ifPresentOrElse(this::cancelReservation, () -> {
                    throw new OperationInacceptableException(String.format("Reservation %s not exits or invalid", reservationId));
                });
    }

    private Reservation createReservation(final ReservationInputDto reservationInputDto, final Creneau creneau) {
        return reservationRepository.save(ReservationMapper
                .toEntity(reservationInputDto, creneau)
                .setStatut(StatutReservation.ACTIVE)
                .setCreneau(creneau.decrementCapacity()));
    }

    private void cancelReservation(final Reservation reservation) {
        reservation
                .setStatut(StatutReservation.CANCELLED)
                .setCreneau(reservation.getCreneau().incrementCapacity());
        reservationRepository.save(reservation);
    }
}
