CREATE TABLE IF NOT EXISTS kata.reservation (
    id UUID NOT NULL DEFAULT gen_random_uuid(),
    accept_consentement boolean not null default false,
    creneau_id UUID NOT NULL,
    email VARCHAR(255) NOT NULL,
    statut VARCHAR(20) NOT NULL CHECK (statut IN('ACTIVE', 'CANCELLED')),
    creation_date timestamptz,
    modification_date timestamptz,
    PRIMARY KEY (id),
    FOREIGN KEY (creneau_id) references kata.creneau(id)
    );