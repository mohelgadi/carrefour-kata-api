CREATE TABLE IF NOT EXISTS kata.creneau (
	id UUID NOT NULL DEFAULT gen_random_uuid(),
    MODE_LIVRAISON VARCHAR(20) NOT NULL CHECK (MODE_LIVRAISON IN('DRIVE', 'DELIVERY', 'DELIVERY_TODAY', 'DELIVERY_ASAP')),
    description VARCHAR(255),
    capacity INTEGER,
    date_debut timestamptz NOT NULL,
    date_fin timestamptz NOT NULL,
	creation_date timestamptz,
	modification_date timestamptz,
	PRIMARY KEY (id)
);